const router = require("express").Router();
const dbController = require('../controller/dbController');
const clienteController = require ("../controller/clienteController")

router.get("/sistemaIngressosOnline/", dbController.getTables);
router.get("/sistemaIngressosOnline/desc", dbController.getTablesDescription);

router.post("/postcliente/", clienteController.createCliente);

module.exports = router;

