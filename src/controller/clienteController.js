const connect = require("../db/connect");

module.exports = class clienteController{

    static async createCliente(req,res){
        const{
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia
        }= req.body;

        if (telefone !== 0) {
            const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
            )`;
            try{

                connect.query(query, function (err){

                    if (err) {
                        console.log(err);
                        res.status(500).json({error: "Usuario não cadastrado no banco!!"});
                        return;
                    }
                    console.log("inserido ao banco");
                    res.status(201).json({message: "Usuario criado com sucesso!!!"})
                }
                ) 
                    
                

            }catch(error){
                console.log("ERROR AO EXECUTAR O INSERT!!", error);
                res.status(500).json({error: "Erro interno do servidor"});

            }
        }
        else{
            res.status(400).json({message: "Telefone e obrigatorio"})
        }

    }// fim do createCliente
}// fim do module